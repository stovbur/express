const path = require('path')
const fs = require('fs')

const pages = require('../data/pages.json')

const normalizePage = (value) => {
  const result = path.normalize(value)
  return result === '.' ? '' : result.replace(/^(.*\/)?index$/, '$1')
}

const getPageName = (page) =>
  page && !page.endsWith('/') ? page : path.join(page, 'index')

const getPageLocals = (pageName) => pages[pageName]

exports.showPage = ({ params: { page } }, res, next) => {
  const normalizedPage = normalizePage(page)
  if (normalizedPage.startsWith('../')) return next()
  const pageName = getPageName(normalizedPage)

  const pageViewFile = path.join('views/pages', `${pageName}.hbs`)
  if (!fs.existsSync(pageViewFile)) return next()

  if (page !== normalizedPage) return res.redirect(`/${normalizedPage}`)

  const view = path.join('pages', pageName)
  const locals = getPageLocals(pageName)
  res.render(view, locals)
}
