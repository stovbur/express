var express = require('express')
var router = express.Router()

const mainController = require('../controllers/mainController')

router.get('/:page(*)', mainController.showPage)

router.get('/hello', function (req, res, next) {
  res.render('hello', { title: 'App' })
})

module.exports = router
